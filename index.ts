import { EventEmitter } from "events";
import { CoinEvent, Coin } from "./monitor";
import {
  addTransaction,
  markTransactionCompleted,
  transacionsCache,
} from "./trader/RecordHandler";
import chalk from "chalk";
import {
  account,
  approve,
  getAmountOut,
  getBalanceByToken,
  tokenswap,
  PriceEvent,
} from "./trader/SwapHandler";
import { BigNumber } from "ethers";
import { ethers } from "ethers";
import Decimal from "decimal.js";
import { BUSD, isDev, USDT, WBNB } from "./lib/common";

const coinEvent = new CoinEvent();
const e = new EventEmitter();
let priceEvent: PriceEvent;
const dryRun = isDev;

/**
 * newCoinHandler.
 *
 * @param {Coin[]} newCoin
 */
const newCoinHandler = (newCoin: Coin[]) => {
  const newestCoin = newCoin[0];
  if (newestCoin.flow && !newestCoin.flow.trusted) {
    return;
  }

  const newCoinInFromCoinCSV = transacionsCache.filter(
    (t) => t.fromCoin === newestCoin.token
  );

  // don't trade twice
  if (newCoinInFromCoinCSV.length) {
    console.log(
      `${chalk.bgRed(`Already traded: ${newestCoin.name} ${newestCoin.token}`)}`
    );
    return;
  }

  // start trading
  e.emit("Trade", newestCoin);
};

e.on("Trade", async (newestCoin: Coin) => {
  coinEvent.stopEvent();
  const suggestedTradingOrder = newestCoin.flow.suggestedPath;
  const altCoin = suggestedTradingOrder[suggestedTradingOrder.length - 1];
  const baseCoin = suggestedTradingOrder[suggestedTradingOrder.length - 2];

  const address = account.address;
  let wbnBAmountIn = await getBalanceByToken(WBNB, address);
  console.log(`balance: ${wbnBAmountIn.toString()}`);

  let actualAmountOut: BigNumber;
  if (dryRun) {
    // test
    wbnBAmountIn = BigNumber.from("1000000000000000");
    let _a: BigNumber;
    if (baseCoin !== WBNB) {
      _a = (await getAmountOut(WBNB, baseCoin, wbnBAmountIn))[1];
      console.log(chalk.bgRed("baseCoin is not WBNB but", baseCoin, _a));
    } else {
      _a = wbnBAmountIn;
    }
    actualAmountOut = (await getAmountOut(baseCoin, altCoin, _a))[1]; //(await checkLiq(suggestedPath[suggestedPath.length-2], suggestedPath[suggestedPath.length-1], BigNumber.from('1000000000000000')))
  } else {
    actualAmountOut = await tokenswap(
      newestCoin.flow.suggestedPath
    );
  }

  // set altCoin to baseCoin amount if the order contains more than 2 coins
  const altToBaseCurrentAmount = (await getAmountOut(
    altCoin,
    baseCoin,
    actualAmountOut
  ))[1];
  console.log(
    "altToBaseCurrentAmount",
    wbnBAmountIn,
    altCoin,
    baseCoin,
    actualAmountOut.toString(),
    altToBaseCurrentAmount.toString()
  );

  console.log(
    chalk.cyan(`actualAmountOut: ${actualAmountOut} ${newestCoin.name}`)
  );
  if (actualAmountOut && !actualAmountOut.isZero()) {
    const oneBNB = ethers.utils.parseUnits("1", "ether");
    const b = await getAmountOut(WBNB, BUSD, oneBNB);
    const bnbPrice = ethers.utils.formatUnits(b[1].toString(), "ether");
    const now = Date.now();
    try {
      const isWritten = await addTransaction({
        fromCoin: WBNB,
        toCoin: altCoin,
        amountIn: wbnBAmountIn.toString(),
        amountOut: actualAmountOut.toString(),
        bnbPrice: bnbPrice,
        time: now,
        completed: false,
      });
      console.log(chalk.bgYellowBright(`isWritten: ${isWritten}`));
    } catch (error) {
      console.error(error);
    }

    if (dryRun) {
      //test
    } else {
      await approve(newestCoin.token, ethers.constants.MaxUint256);
    }

    priceEvent = new PriceEvent();
    priceEvent.startAmountUpdater(
      suggestedTradingOrder,
      actualAmountOut,
      altToBaseCurrentAmount,
      now
    );
    priceEvent.on("amountUpdated", checkSellCondition);
  }
});


/**
 * sell.
 *
 * @param {BigNumber} altCoinTradedAmount
 * @param {BigNumber} amountNow
 * @param {string[]} ascendingSuggestedOrder
 */
const sell = async (
  altCoinTradedAmount: BigNumber, // actualAmountOut
  amountNow: BigNumber,
  ascendingSuggestedOrder: string[] // suggestedPath
) => {
  let reversedTradingOrder = ascendingSuggestedOrder;
  reversedTradingOrder = reversedTradingOrder.reverse();
  priceEvent.removeAllListeners("amountUpdated");
  priceEvent.stopAmountUpdater();

  let actualSellOutAmount: BigNumber;
  if (dryRun) {
    let _a: BigNumber = altCoinTradedAmount;
    for (var i = 1, len = reversedTradingOrder.length; i < len; i++) {
      _a = (
        await getAmountOut(
          reversedTradingOrder[i - 1],
          reversedTradingOrder[i],
          _a
        )
      )[1];
      console.log(reversedTradingOrder[i - 1], reversedTradingOrder[i], _a.toString());
    }
    actualSellOutAmount = _a;
    console.log(actualSellOutAmount);
  } else {
    actualSellOutAmount = await tokenswap(
      reversedTradingOrder
    );
  }

  if (actualSellOutAmount && !actualSellOutAmount.isZero()) {
    const oneBNB = ethers.utils.parseUnits("1", "ether");
    const bnbAmount = await getAmountOut(WBNB, BUSD, oneBNB);
    const bnbPrice = ethers.utils.formatUnits(bnbAmount[1], "ether");
    const now = Date.now();
    try {
      const isWritten = await addTransaction({
        fromCoin: reversedTradingOrder[0],
        toCoin: WBNB,
        amountIn: altCoinTradedAmount.toString(),
        amountOut: actualSellOutAmount.toString(),
        bnbPrice: bnbPrice,
        time: now,
        completed: false,
      });
      console.log(chalk.bgYellowBright(`isWritten: ${isWritten}`));
    } catch (error) {
      console.error(error);
    }

    await markTransactionCompleted(reversedTradingOrder[0]);
  }
  return actualSellOutAmount;
};


/**
 * checkSellCondition.
 *
 * @param {BigNumber} altCoinTradedAmount
 * @param {BigNumber} altToBaseOldAmount
 * @param {BigNumber[]} amountPair
 * @param {string[]} tradingOrder
 * @param {number} tradingTime
 */
const checkSellCondition = async (
  altCoinTradedAmount: BigNumber, // actualAmountOut
  altToBaseOldAmount: BigNumber,
  amountPair: BigNumber[],
  tradingOrder: string[], // suggestedPath
  tradingTime: number
) => {
  const benefitPercentage = 1.25; // amount percentage below 1 = price go up
  const losePercentage = 0.95;
  const utilAfter = 30; // in minute

  const loseAmountTarget = new Decimal(altToBaseOldAmount.toString()).mul(
    losePercentage
  );
  const benefitAmountTarget = new Decimal(altToBaseOldAmount.toString()).mul(
    benefitPercentage
  );
  const currentAmount = new Decimal(amountPair[1].toString())//mul(new Decimal('5000000000000000'));
  const sellOutTimeOut = new Date(tradingTime + utilAfter * 60000);
  console.log(
    `upper: ${benefitAmountTarget.toString()}, lower: ${loseAmountTarget.toString()}, current: ${currentAmount.toString()} ${amountPair[1].toString()}`
  );
  // console.log(`time out: ${sellOutTimeOut.getTime()}, current: ${Date.now()}`);

  const callSell = async () =>
    sell(altCoinTradedAmount, amountPair[1], tradingOrder);

  // sell conditions
  if (currentAmount.lt(loseAmountTarget)) {
    const a = await callSell();
    console.log(
      chalk.bgGreen(
        `${tradingOrder} sold and get ${a.toString()}`
      )
    );
    coinEvent.startEvent();
  }

  if (currentAmount.gt(benefitAmountTarget)) {
    const a = await callSell();
    console.log(
      chalk.bgRed(
        `${tradingOrder} sold and get ${(a.toString())}`
      )
    );
    coinEvent.startEvent();
  }

  if (Date.now() > sellOutTimeOut.getTime()) {
    const a = await callSell();
    console.log(
      chalk.bgBlue(
        `${tradingOrder} timed out, sold and get ${(a.toString())}`
      )
    );
    coinEvent.startEvent();
  }
};

coinEvent.init(1);
coinEvent.on("newCoin", newCoinHandler);

// if (dryRun) {
//   const c: Coin = {
//     id: "22856",
//     chain: "",
//     token: "0x43a8e7bd081a5217a4ac1be97632ada39da735ed",
//     name: "PLG",
//     symbol: "PLG",
//     flow: {
//       suggestedPath: [WBNB, USDT, "0x43a8e7bd081a5217a4ac1be97632ada39da735ed"],
//       trusted: true,
//     },
//   };

//   setTimeout(() => {
//     coinEvent.emit("newCoin", [c]);
//   }, 1000);
// }
