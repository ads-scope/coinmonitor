import { fileURLToPath } from 'url';
import { HTMLElement } from "node-html-parser";
import dotenv from 'dotenv';
import * as path from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


function tableToJSON(table: HTMLElement) {
	const headers = Array.from(table.querySelectorAll('thead tr th')).map(el => el.textContent.trim());
	const result = [];
	const rows = Array.from(table.querySelectorAll('tbody tr'));
	rows.forEach(row => {
		const cells = Array.from(row.querySelectorAll('td')).map(el => el.textContent.trim());
		result.push(headers.reduce((rowRes, header, headerIndex) => {
			return {
				...rowRes,
				[header]: cells[headerIndex]
			};
		}, {}));
	});
	return result;
}

export interface AvailablePair {
	'#': string;
	Exchange: string;
	Pair: string;
	Price: string;
	Spread: string;
	'+2% Depth': string;
	'-2% Depth': string;
	'24h Volume': string;
	'Volume %': string;
	'Last Traded': string;
	'Trust Score': string;
}

export interface BestPairResult {
	suggestedPath: string[];
	trusted: boolean;
}


const isDev = false;

// function loadConfig(){
if (isDev) {
	dotenv.config({ path: path.join(__dirname, '../.env.testnet') });
} else {
	dotenv.config({ path: path.join(__dirname, '../.env.example') });
}
const data = {
	WBNB: process.env.WBNB_CONTRACT, //wbnb
	BUSD: process.env.BUSD_CONTRACT,
	USDT: process.env.USDT_CONTRACT,
}
// }

const WBNB = data.WBNB;
const BUSD = data.BUSD;
const USDT = data.USDT;

export { tableToJSON, isDev, WBNB, BUSD, USDT }

