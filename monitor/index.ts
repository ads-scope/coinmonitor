import { parse } from "node-html-parser";
import puppeteer from "puppeteer-extra";
import StealthPlugin from "puppeteer-extra-plugin-stealth";
import { PuppeteerBlocker } from "@cliqz/adblocker-puppeteer";
import fetch from "cross-fetch";
import { fileURLToPath } from "url";
import path from "path";
import notifier from "node-notifier";
import { EventEmitter } from "events";
import {
  AvailablePair,
  tableToJSON,
  BestPairResult,
  USDT,
  WBNB,
  BUSD,
  isDev,
} from "../lib/common";
import chalk from "chalk";
import {BrowserConnectOptions, BrowserLaunchArgumentOptions, LaunchOptions} from "puppeteer";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


let browserOption:LaunchOptions & BrowserLaunchArgumentOptions & BrowserConnectOptions  = {
    headless: true,
    args: [
        `--user-data-dir=${path.normalize(path.join(__dirname, "/profile"))}`,
        `--window-size=${900},${900}`,
        "--disable-gpu",
        "--disable-setuid-sandbox",
        "--no-sandbox",
        "--no-zygote",
    ],
};

const isX86 = process.arch === 'x64' || process.arch === 'x86'
if (!isX86) {
    browserOption.executablePath= '/usr/bin/chromium';
}
if (process.platform === 'linux'){
    browserOption.args.push('--disk-cache-dir=/tmp/chromium')
}

puppeteer.use(StealthPlugin());
const blocker = await PuppeteerBlocker.fromPrebuiltAdsAndTracking(fetch);
blocker.blockFonts().blockFrames().blockImages().blockStyles();
const browser = await puppeteer.launch(browserOption);

export class Coin {
  name: string;
  symbol: string;
  chain: string;
  token: string;
  id: string;
  flow?: BestPairResult;
  constructor(
    name: string,
    symbol: string,
    chain: string,
    token: string,
    id: string
  ) {
    this.name = name;
    this.symbol = symbol;
    this.chain = chain;
    this.token = token;
    this.id = id;
  }
}

async function getCoingeckoRecentAddedContent() {
  const pages = await browser.pages();

  let res;
  try {
    if (
      pages.some(
        (v) => v.url() === "https://www.coingecko.com/en/coins/recently_added"
      )
    ) {
      const targetPage = pages.filter(
        (v) => v.url() === "https://www.coingecko.com/en/coins/recently_added"
      )[0];
      res = await targetPage.reload();
    } else {
      const page = await browser.newPage();
      await page.setCacheEnabled(false);
      await blocker.enableBlockingInPage(page);
      const blocker2 = await PuppeteerBlocker.fromPrebuiltAdsAndTracking(fetch);
      blocker2.blockScripts();
      blocker2.enableBlockingInPage(page);
      res = await page.goto(
        "https://www.coingecko.com/en/coins/recently_added",
        {
          waitUntil: "networkidle0",
          timeout: 0,
        }
      );
    }
    return await res.text();

  } catch (error) {
    console.error('failed to get recently added coins');
    return await getCoingeckoRecentAddedContent();
  }
}

async function getRecentlyAddedCoins() {
  const res: Coin[] = await getCoingeckoRecentAddedContent()
    .then((res) => res)
    .then(parse)
    .then((res) =>
      res.querySelectorAll(
        'table[data-target="gecko-table.table portfolios-v2.table"] >tbody> tr'
      )
    )
    .then((res) =>
      res.map((v) => {
        let chain = "",
          name = "",
          symbol = "",
          token = "",
          id = "";
        if (
          v.querySelectorAll('td[data-sort="BNB Smart Chain"]').length > 0
        )
          chain = "BSC";
        if (v.querySelectorAll('td[data-sort="Ethereum"]').length > 0)
          chain = "ETH";

        name = v.querySelector(".coin-name").getAttribute("data-sort");
        symbol = v
          .querySelector(".tw-hidden.d-lg-inline.font-normal.text-3xs.ml-2")
          .innerHTML.replace(/[ \n]/g, "");

        const favButton = v.querySelector(
          "[data-unfavorite-coin-confirm-button]"
        );
        id = favButton.getAttribute("data-coin-id");
        // console.log(v.querySelector('#dropdownMenuButton'))
        const addressEle = v.querySelector("#dropdownMenuButton");
        if (addressEle) {
          token = addressEle
            .querySelector("i[data-address]")
            .getAttribute("data-address");
        }

        return new Coin(name, symbol, chain, token, id);
      })
    );
  return res;
}

class CoinEvent extends EventEmitter {
  private previousRecentlyAddedCoins: string[] = [];
  private numberOfNewCoinToBeReturned: number;
  // eslint-disable-next-line no-undef
  private timeoutHandler: NodeJS.Timeout = null;
  public init(numberOfNewCoinToBeReturned: number) {
    this.numberOfNewCoinToBeReturned = numberOfNewCoinToBeReturned;
    this.startEvent();
  }

  public stopEvent() {
    const t = this.timeoutHandler;
    clearTimeout(t);
  }

  public startEvent() {
    setTimeout(() => {
      this.refresh(this.numberOfNewCoinToBeReturned);
      this.timeoutHandler = setTimeout(() => {
        this.startEvent();
      }, 10000 + Math.random() * 5000);
    }, 0);
  }

  private async refresh(numberOfNewCoinToBeReturned: number) {
    const recentlyAddedCoins = await getRecentlyAddedCoins();
    // console.table(recentlyAddedCoins);
    let newCoins: Coin[];
    if (!isDev) {
      newCoins = this.previousRecentlyAddedCoins.length
        ? recentlyAddedCoins.filter(
            (v) => !this.previousRecentlyAddedCoins.includes(v.symbol)
          )
        : [];
    } else {
      newCoins = recentlyAddedCoins; //.filter(v => !this.previousRecentlyAddedCoins.includes(v.symbol));
    }
    newCoins = newCoins.filter((v) => v.chain === "BSC");

    // console.log(recentlyAddedCoins);
    if (Math.random() > 0.6) {
      console.log(
        `${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}`
      );
    }

    if (!!newCoins.length) {
      const coinFlows = newCoins.slice(0, numberOfNewCoinToBeReturned);
      // Promise.all(coinFlows.map(v=>))

      if (coinFlows.length) {
        //  single item first
        try {
          const flow = await getTradingFlowFromGeckocoin(
            coinFlows[0].token,
            coinFlows[0].id
          );
          newCoins[0].flow = flow;
          console.log(chalk.bgGray(JSON.stringify(newCoins.slice(0, 5))));
          this.emit("newCoin", newCoins);
          notifier.notify({
            title: "My notification",
            message: newCoins.map((c) => c.symbol).join(", \n"),
            wait: false,
            sound: true,
            icon: undefined,
          });
        } catch (error) {
          console.error(error);
        }
      }
    }

    this.previousRecentlyAddedCoins = recentlyAddedCoins.map((v) => v.symbol);
  }
}

async function getTradingFlowFromGeckocoin(
  token: string,
  geckocoinCoinId: string
) {
  const coinPairPage = await browser.newPage();
  await coinPairPage.setCacheEnabled(false);
  await blocker.enableBlockingInPage(coinPairPage);

  const r = await coinPairPage.goto(
    `https://www.coingecko.com/en/coins/${geckocoinCoinId}/top_tickers`,
    {
      waitUntil: "networkidle0",
      timeout: 0,
    }
  );
  const rText = parse(await r.text());
  const t = rText.querySelector('[data-target="gecko-table.table"]');
  const tableJson = tableToJSON(t) as AvailablePair[];
  let sortedTableJson = tableJson.sort((a, b) =>
    Number.parseInt(a["Volume %"].split("%")[0]) >
      Number.parseInt(b["Volume %"].split("%")[0]) &&
    a.Exchange.indexOf("Pancake") > -1
      ? -1
      : 1
  );
  // Only connect to PancakeSwap
  sortedTableJson = sortedTableJson.filter(v => v.Exchange.indexOf('Pancake') > -1);
  const firstItem = sortedTableJson[0];
  console.log(firstItem);

  const pairColumn = firstItem.Pair.split("\n"); // 'RDR/BUSD\n\nLive Chart\n0x92da433da84d58dfe2aade1943349e491cbd6820'
  const pairString = pairColumn[0];

  // suggested trading order
  let suggestedPath: string[] = [];
  let trusted = false;
  // if (pairString.indexOf("USDT") > -1) {
  //   suggestedPath = [WBNB, USDT, token];
  //   trusted = true;
  // } 
  // if (pairString.indexOf("BUSD") > -1) {
  //   suggestedPath = [WBNB, BUSD, token];
  //   trusted = true;
  // } 
  if (pairString.indexOf("WBNB") > -1) {
    suggestedPath = [WBNB, token];
    trusted = true;
  }

  const res: BestPairResult = {
    suggestedPath: suggestedPath,
    trusted: trusted,
  };

  coinPairPage.close();
  return res;
}

// const coinEvent = new CoinEvent();
// coinEvent.init(1);

// coinEvent.on("newCoin", (newCoins) => {
//   console.log("coin\n" + JSON.stringify(newCoins));
// });

export { CoinEvent, getTradingFlowFromGeckocoin };
