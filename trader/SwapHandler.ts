import { EventEmitter } from "events";
import { TransactionReceipt } from "@ethersproject/providers";
import { ethers, BigNumber } from "ethers";
import * as path from "path";

import { readFileSync } from "fs";
import { fileURLToPath } from "url";
import chalk from "chalk";
import { BUSD, isDev, USDT, WBNB } from "../lib/common";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const file = readFileSync(path.join(__dirname, "onlyone-abi.json"), {
  encoding: "utf8",
  flag: "r",
});
const pancakeswapABI = readFileSync(
  path.join(__dirname, "pancakeswap-abi.json"),
  { encoding: "utf8", flag: "r" }
);
const abi = JSON.parse(file);

const data = {
  factory: process.env.FACTORY, //PancakeSwap V2 factory

  router: process.env.ROUTER, //PancakeSwap V2 router
  privateKey: process.env.PRIVATE_KEY,
};

const wss =
  "wss://speedy-nodes-nyc.moralis.io/672bdbae1a2bd1837c78a16e/bsc/mainnet/ws";
const https =
  "https://speedy-nodes-nyc.moralis.io/672bdbae1a2bd1837c78a16e/bsc/mainnet";
const bscHttps = "https://bsc-dataseed1.ninicoin.io/";
const testnetHttps = "https://data-seed-prebsc-2-s2.binance.org:8545/";

//First address of this mnemonic must have enough BNB to pay for tx fess
let provider;
if (isDev) {
  provider = new ethers.providers.JsonRpcProvider(testnetHttps);
} else {
  provider = new ethers.providers.JsonRpcProvider(bscHttps);
}
// const provider = new ethers.providers.JsonRpcProvider(https);
const w = new ethers.Wallet(data.privateKey);
const account = w.connect(provider);

/**
 *
 * @param token
 * @param address
 * @returns balance
 */
async function getBalanceByToken(token: string, address: string) {
  const balanceIns = new ethers.Contract(token, abi, provider);
  const balance: BigNumber = await balanceIns.balanceOf(address); // my public address

  return balance;
}

const router = new ethers.Contract(
  data.router,
  [
    "function getAmountsOut(uint amountIn, address[] memory path) public view returns (uint[] memory amounts)",
    "function swapExactTokensForTokens(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external returns (uint[] memory amounts)",
  ],
  account
);

const factory = new ethers.Contract(
  data.factory,
  [
    "event PairCreated(address indexed token0, address indexed token1, address pair, uint)",
    "function getPair(address tokenA, address tokenB) external view returns (address pair)",
  ],
  account
);

/**
 * getApprovalContract.
 *
 * @param {string} token
 */
function getApprovalContract(token: string) {
  return new ethers.Contract(
    token,
    ["function approve(address spender, uint amount) public returns(bool)"],
    account
  );
}

let gasLimit = 290000;
let gas = 5000000000; //await router.provider.getGasPrice();
if (isDev) {
  gasLimit = 290000;
  gas = 10000000000; //ethers.utils.parseUnits('10', 'Gwei')
}

console.log(chalk.grey(`${gasLimit}, ${gas}`));
// let tokenIn = addresses.WBNB, tokenOut = addresses.MYTOKEN;
// const amountIn = ethers.utils.parseUnits('0.001', 'ether');

/**
 *
 * @param tokenIn
 * @param amountIn the amount to be approved
 * @returns true if ok
 */
const approve = async (tokenIn: string, amountIn: BigNumber) => {
  try {
    const txApprove = await getApprovalContract(tokenIn).approve(
      router.address,
      amountIn
    );
    let receipt = await txApprove.wait();
    console.log(`Approve ${tokenIn} for receipt ${account.address}`);
    console.log(receipt);

    return true;
  } catch (error) {
    console.log("### Error ######");
    throw "Approval failed\n" + error;
  }
};

/**
 *
 * @returns the amount swapped in pancakeswap
 */
const tokenswap = async (tokens: string[]) => {
  if (tokens.length < 2) {
    console.log(chalk.redBright(`token chain is less than 2`));
    return null;
  }

  const amountIn = await getBalanceByToken(tokens[0], account.address);
  console.log('amountIn ######');
  console.log(amountIn);

  let _a = amountIn;
  for (let i = 0; i + 1 < tokens.length; i++) {
    _a = (await getAmountOut(tokens[i], tokens[i + 1], _a))[1];
  }

  // const amounts: BigNumber[] = await router.getAmountsOut(amountIn, [
  //   tokens[tokens.length - 2],
  //   tokens[tokens.length - 1],
  // ]);
  console.log("amounts $$$$$$");
  console.log(_a.toString());
  console.log(ethers.utils.formatUnits(_a));
  //Our execution price will be a bit different, we need some flexbility
  const amountOutMin = _a.sub(_a.div(10));
  console.log(
    `Buying new token ================= tokenIn: \n${amountIn.toString()} ${chalk.cyan(
      tokens.toString()
    )} \n tokenOut: ${amountOutMin.toString()} ${tokens[tokens.length - 1]}`
  );
  // console.log('##### gas #######');
  // console.log(ethers.utils.parseUnits(gas.toString(), 'ether'));

  const pair = await checkLiq(
    tokens[tokens.length - 2],
    tokens[tokens.length - 1],
    _a
  );
  console.log(`${tokens[tokens.length - 1]} of ${pair}`);
  if (pair && pair.isZero()) {
    console.log(`Not enough liq : min: ${amountOutMin}`);
    return null;
  }

  try {
    const nonce = await router.provider.getTransactionCount(account.address);
    const tx = await router.swapExactTokensForTokens(
      amountIn,
      amountOutMin,
      [...tokens],
      account.address,
      Date.now() + 1000 * 60 * 10, //10 minutes
      {
        nonce: nonce,
        gasLimit: gasLimit,
        gasPrice: gas,
      }
    );
    const receipt1: TransactionReceipt = await tx.wait();
    console.log("Swapped");
    // console.log('Transaction receipt of tx');
    // console.log(receipt1);
    return getActualAmountOut(receipt1);
  } catch (err) {
    console.log("### Error ######");
    console.log(err);
  }
};

/**
 * getActualAmountOut.
 *
 * @param {TransactionReceipt} t
 */
const getActualAmountOut = (t: TransactionReceipt) => {
  // const amountsOut2 = await provider.getTransactionReceipt('0x5451ff14bd4c4d460f203eab7e2fdcd0558aeb3c8a7bb0effa4d07df8ec1182a');
  const inter = new ethers.utils.Interface(pancakeswapABI);
  const lastReceipt = inter.parseLog(t.logs[t.logs.length - 1]); //	last one = swap event
  const amount0Out: BigNumber = lastReceipt.args["amount0Out"];
  const amount1Out: BigNumber = lastReceipt.args["amount1Out"];
  // console.log(!amount0Out.isZero() ? amount0Out.toString() : amount1Out.isZero() ? amount1Out.toString() : '0')
  return !amount0Out.isZero()
    ? amount0Out
    : !amount1Out.isZero()
    ? amount1Out
    : BigNumber.from(0);
};

/**
 *
 * @param tokenIn
 * @param tokenOut
 * @param amountIn
 * @returns [tokenIn amount, tokenOut amount] length always = 2
 */
const getAmountOut = async (
  tokenIn: string,
  tokenOut: string,
  amountIn: BigNumber
) => {
  const amount: BigNumber[] = await router.getAmountsOut(amountIn, [
    tokenIn,
    tokenOut,
  ]);
  return amount;
};

/**
 * PriceEvent.
 *
 * @extends {EventEmitter}
 */
class PriceEvent extends EventEmitter {
  /**
   * @type {NodeJS.Timer}
   */
  private interval: NodeJS.Timer = null;
  /**
   * startAmountUpdater.
   *
   * @param {string[]} tokens
   * @param {BigNumber} altCoinAmount
   * @param {BigNumber} altToBaseOldAmount
   * @param {number} time
   */
  public startAmountUpdater(
    tokens: string[],
    altCoinAmount: BigNumber,
    altToBaseOldAmount: BigNumber,
    time: number
  ) {
    //check price after traded
    this.interval = setInterval(async () => {
      const altCoin = tokens[tokens.length - 1];
      const baseCoin = tokens[tokens.length - 2];
      const amountPair = await getAmountOut(altCoin, baseCoin, altCoinAmount);
      if (isDev) {
        console.log(
          "interval ",
          JSON.stringify(tokens),
          altCoin,
          baseCoin,
          altToBaseOldAmount.toString(),
          altCoinAmount.toString(),
          amountPair[1].toString()
        );
      }
      this.emit(
        "amountUpdated",
        altCoinAmount,
        altToBaseOldAmount,
        amountPair,
        tokens,
        time
      );
    }, 1000);
  }

  /**
   * stopAmountUpdater.
   */
  public stopAmountUpdater() {
    clearInterval(this.interval);
  }
}

/**
 * @param amountOut =assumed amountOut
 */
let checkLiq = async (
  tokenIn: string,
  tokenOut: string,
  amountOut: BigNumber
) => {
  const pairAddressx: string = await factory.getPair(tokenIn, tokenOut);
  console.log(chalk.blue(`${tokenIn} to ${tokenOut}, PairAddress: ${pairAddressx}`));
  if (pairAddressx !== null && pairAddressx !== undefined) {
    // console.log("pairAddress.toString().indexOf('0x0000000000000')", pairAddress.toString().indexOf('0x0000000000000'));
    if (pairAddressx.toString().indexOf("0x0000000000000") > -1) {
      console.log(
        chalk.red(`PairAddress ${pairAddressx} not detected. Restarting...`)
      );
      // return await run();
    }
    const liq = await getBalanceByToken(tokenOut, pairAddressx);

    const ratio = liq.div(amountOut);
    if (amountOut.gte(liq) || ratio.lte(10000)) {
      // normally the liquidity is more than 100 times of the amount if not it may be not a correct pair
      console.log(
        chalk.redBright(
          `Not enough liquidity:\namount: ${amountOut}\nLiq: ${liq}`
        )
      );
      return BigNumber.from("0");
    } else {
      console.log(chalk.blue(`Pool Available Ratio: ${liq}/${amountOut} = ${ratio}`));
      return liq;
    }
  }
  return BigNumber.from("0");
  // const pairBNBvalue = await erc.balanceOf(pairAddressx);
  // jmlBnb = ethers.utils.formatEther(pairBNBvalue);
  // console.log(`Value in BNB : ${jmlBnb}`);

  // if (jmlBnb > data.minBnb) {
  // 	setTimeout(() => buyAction(), 3000);
  // } else {
  // 	initialLiquidityDetected = false;
  // 	console.log("Running again...");
  // 	return await run();
  // }
};

/**
 * init.
 */
const init = async () => {
  console.log("Transaction receipt of bnb");
  // console.log(receipt);
  // tokenswap();
  const address = w.address;
  console.log(address);
  // const balance = await account.getBalance()
  // console.log(balance.toString());
  // const bscIns = new ethers.Contract('0x0E09FaBB73Bd3Ade0a17ECC321fD13a19e81cE82', abi, bscProvider);
  try {
    // const myBNBBalance = await account.getBalance();
    // const balance = await getBalanceByToken(data.WBNB, address);
    // console.log(`BNB balance: ${balance}`);
    // console.log(`BNB balance: ${myBNBBalance}`);
    // const fee = await provider.getGasPrice();
    // console.log(`fee: ${ethers.utils.formatUnits(fee, 'wei')}`);

    // get token price
    // const timer = setInterval(async () => {
    // const tokenIn = '0x34550001fbf7d6e42e812763c91ef96f129742ab';
    // const amountsOut = await router.getAmountsOut(ethers.utils.parseUnits('1', 'ether'), [data.WBNB, tokenIn]);
    // const amountsIn = await router.getAmountsIn(ethers.utils.parseUnits('1', 'ether'), ['0x6ad0f087501eee603aeda0407c52864bc7f83322', data.WBNB]);
    // const bnbAmounts: BigNumber = await router.getAmountsOut(ethers.utils.parseUnits('1', 'ether'), [data.WBNB, data.BUSD]);
    // console.log(`bnb amount: ${ethers.utils.formatUnits(bnbAmounts[1].toString(), 'wei')}`)
    // console.log(`bnb amount: ${ethers.utils.formatUnits(amountsOut[1].toString(), 'wei')}`)
    // const tokenPerBusdPrice = new Decimal(bnbAmounts[1].toString()).dividedBy(new Decimal(amountsOut[1].toString()));
    // console.log(`${tokenIn}/BNB: ${tokenPerBusdPrice}`);

    // testnet
    // const amountsOut2 = await provider.getTransactionReceipt('0x16371c286a0849a7ad4636dc50d50e6860fb1135a4643aa15568ba467313c619');
    // console.log((amountsOut2))

    const testCoin = "0x5b6bd1ad8cf355c22df1323822cdc66afcfa87bb";
    const out = BUSD;
    // const testCoin = '0x8a9424745056eb399fd19a0ec26a14316684e274'; // testnet dai token

    console.log(chalk.white(`check liq start`));
    // const pair = await checkLiq(
    //   testCoin,
    //   BUSD,
    //   ethers.utils.parseUnits("0.001", "ether")
    // );
    // console.log(`liq ${pair}`);

    console.log(
      (
        await getAmountOut(testCoin, out, ethers.utils.parseUnits("1", "ether"))
      )[1].toString()
    );
    const t = (
      await getAmountOut(out, testCoin, ethers.utils.parseUnits("1", "ether"))
    )[1].toString();

    console.log(t);
    console.log(
      (await getAmountOut(testCoin, out, BigNumber.from(t)))[1].toString()
    );
    console.log(chalk.white(`check liq end`));

    console.log(chalk.redBright(`test swapping multi coins start`));
    // await approve(data.WBNB, ethers.constants.MaxUint256)
    // const v= ethers.utils.parseUnits('0.0001', 'ether');
    // const t =await tokenswap(BigNumber.from( v), data.WBNB, data.BUSD,testCoin) //, '0x84b9b910527ad5c03a9ca831909e21e236ea7b06'
    // console.log(ethers.utils.formatUnits(t, 'gwei'));
    console.log(chalk.redBright(`test swapping multi coins end`));

    const amountIn = await getBalanceByToken(WBNB, account.address);
    const order = [WBNB, BUSD, testCoin];
    let _a = amountIn;
    let last2Amount = amountIn;
    for (let i = 0; i + 1 < order.length; i++) {
      last2Amount = _a;
      _a = (await getAmountOut(order[i], order[i + 1], _a))[1];
    }
    console.log(_a.toString());
    console.log(last2Amount.toString())
  } catch (error) {
    console.error(error);
  }
};

// init();

export {
  tokenswap,
  approve,
  getBalanceByToken,
  getAmountOut,
  account,
  PriceEvent,
  checkLiq,
};
