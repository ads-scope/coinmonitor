import { write, FormatterRow, parseStream, parse, parseString } from 'fast-csv';
import * as fs from 'fs';

interface TransactionRecord {
  amountIn: string,
  amountOut: string,
  bnbPrice: string,
  completed: boolean, // transfer back as bnb
  fromCoin: string,
  time: number,
  toCoin: string,
}

const TRANSACTION_RECORD_FILENAME = 'transaction.csv';
var transacionsCache: TransactionRecord[] = [];

const writer = async (filename: string, arr: FormatterRow[], flags = 'a', remainHeaders = fs.existsSync(filename) ? false : true) => {
  return new Promise<boolean>((res, rej) => {
    const ws = fs.createWriteStream(filename, { flags: flags });
    write(arr, {
      headers: remainHeaders,
      includeEndRowDelimiter: true
    })
      .pipe(ws)
      .on('error', error => { console.error(error); rej(error) })
      .on('finish', r => res(true));
  })
}

const reader = async (filename: string) => {
  const a = [];
  const rs = fs.readFileSync(filename, { flag: 'a+' }).toString('utf-8')
  const r = parseString(rs, { headers: true });
  return new Promise<TransactionRecord[]>((res, rej) => {
    r.on('data', (d) => a.push(d))
      .on('end', () => res(a))
  })
}

const transactionRecordWriter = async (arr: TransactionRecord[]) => {
  return await writer(TRANSACTION_RECORD_FILENAME, arr);
}

const transactionRecordReader = async (filename: string) => {
  return await reader(filename);
}

const addTransaction = async (record: TransactionRecord): Promise<Boolean> => {
  const isWritten = await transactionRecordWriter([record]);
  if (isWritten) {
    const r = await reader(TRANSACTION_RECORD_FILENAME);
    if (!transacionsCache.length) transacionsCache = r;

    const latestRecord = r.pop();
    if (latestRecord && latestRecord.amountIn === record.amountIn) {
      transacionsCache.push(latestRecord);
      return true;
    }
  }
  return false;
}

const getPastSwappedTokens = async () => {
  if (!transacionsCache.length) {
    const r = await reader(TRANSACTION_RECORD_FILENAME);
    transacionsCache = r
  };
  return [...new Set(transacionsCache.map(v => v.toCoin))];
}

const markTransactionCompleted = async (token: string) => {
  // const csv = await transactionRecordReader(TRANSACTION_RECORD_FILENAME);
  for (const i in transacionsCache) {
    let item = transacionsCache[i];
    if (item.fromCoin === token || item.toCoin === token) {
      transacionsCache[i].completed = true;
    }
  }
  return await writer(TRANSACTION_RECORD_FILENAME, transacionsCache, 'w', true);
}

if (!fs.existsSync(TRANSACTION_RECORD_FILENAME))
  await transactionRecordWriter([{ toCoin: '0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c', fromCoin: '0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c', amountIn: '0', time: Date.now(), bnbPrice: '0', amountOut: '0', completed: true }]);

const r = await reader(TRANSACTION_RECORD_FILENAME);
transacionsCache = r;

export { transactionRecordReader, transactionRecordWriter, addTransaction, transacionsCache, getPastSwappedTokens, markTransactionCompleted }